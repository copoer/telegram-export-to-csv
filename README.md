# Telegram Export To CSV Converter

A simple web tool to convert JSON file exported from telegram to a CSV file.

Checkout live version here: https://telegram.andchill.xyz/
